

This application is an Example for using Spring-Boot with Akka and Cassandra as persistence journal.

This project is based on the DDDwithActors Account Example from Vaughn Vernon: https://github.com/VaughnVernon/DDDwithActors

And the spring-boot with akka example is based on Aliaksandr Liakh his implementation https://www.linkedin.com/pulse/spring-boot-akka-part-1-aliaksandr-liakh

For now clean your cassandra database each time you do the run :) 

TBD: Snapshots and Simple Rest resource for creating the accounts

Simply run SpringBootAkkaApplication class from your idea or use java -jar SpringBootAkkaApplication.jar from your commandline to start the application