package com.baasie.examples.springbootakka.actor

/**
  * Created by Kenny on 25-Feb-17.
  */
case class OpenAccountCommand(accountNumber: String, initialBalance: Int)
case class DepositFundsCommand(amount: Int)
case class WithdrawFundsCommand(amount: Int)

case class AccountOpenedEvent(accountNumber: String, initialBalance: Int)
case class FundsDepositedEvent(accountNumber: String, amount: Int)
case class FundsWithdrawnEvent(accountNumber: String, amount: Int)

case class AccountState(accountNumber: String, balance: Int) {
  def fromDeposited(amount: Int): AccountState =
  AccountState(accountNumber, balance + amount)

  def fromWithdrawn(amount: Int): AccountState =
  AccountState(accountNumber, balance - amount)
}
