package com.baasie.examples.springbootakka.actor;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.baasie.examples.springbootakka.configuration.SpringExtension;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * Created by Kenny on 22-Feb-17.
 */
@Component("accountService")
@Scope("prototype")
@Slf4j
public class AccountService extends UntypedActor {

    private SpringExtension springExtension;

    private ActorRef accountActor;

    public AccountService(SpringExtension springExtension) {
        Assert.notNull(springExtension);
        this.springExtension = springExtension;
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if(message instanceof ManageAccount) {
            log.info("Managing account with path {}", getSelf().path());
            ManageAccount manageAccount = (ManageAccount) message;
            accountActor = getContext().actorOf(springExtension.propsCreate("accountActor"), manageAccount.accountNumber());
            log.info("Managing Account {}", manageAccount.accountNumber());
            accountActor.tell(new OpenAccountCommand(manageAccount.accountNumber(), 500), getSelf());
        } else if(message instanceof AccountOpenedEvent) {
            AccountOpenedEvent accountOpenedEvent = (AccountOpenedEvent) message;
            log.info("Account {} Openend with initial Balance {}.", accountOpenedEvent.accountNumber(), accountOpenedEvent.initialBalance());
            accountActor.tell(new DepositFundsCommand(100), getSelf());
        } else if(message instanceof FundsDepositedEvent) {
            FundsDepositedEvent withdrawFunds = (FundsDepositedEvent) message;
            log.info("FundDeposited for {} with amount {}.", withdrawFunds.accountNumber(), withdrawFunds.amount());
            accountActor.tell(new WithdrawFundsCommand(200), getSelf());
        } else if(message instanceof  FundsWithdrawnEvent) {
            FundsWithdrawnEvent depositFunds = (FundsWithdrawnEvent) message;
            log.info("Funds Withdraw for account {} with amount {}.", depositFunds.accountNumber(), depositFunds.amount());
        }
         else {
            log.info("Message Unhandled.");
            unhandled(message);
        }


    }
}
