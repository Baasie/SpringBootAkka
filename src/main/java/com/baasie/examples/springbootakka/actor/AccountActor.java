package com.baasie.examples.springbootakka.actor;

import akka.persistence.UntypedPersistentActor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by Kenny on 22-Feb-17.
 */
@Component("accountActor")
@Scope("prototype")
@Slf4j
public class AccountActor extends UntypedPersistentActor {

    @Override
    public String persistenceId() { return getSelf().path().name(); }

    private AccountState accountState;

    @Override
    public void onReceiveRecover(Object message) {
        if(message instanceof AccountOpenedEvent) {
            updateWith((AccountOpenedEvent) message);
        } else if(message instanceof FundsDepositedEvent) {
            updateWith((FundsDepositedEvent) message);
        } else if (message instanceof FundsWithdrawnEvent) {
            updateWith((FundsWithdrawnEvent) message);
        } else {
            unhandled(message);
        }
    }

    @Override
    public void onReceiveCommand(Object message) {
        if (message instanceof OpenAccountCommand) {
            log.info("Open account with path {}", getSelf().path());
            final OpenAccountCommand openAccount = (OpenAccountCommand) message;
            AccountOpenedEvent accountOpenedEvent = new AccountOpenedEvent(openAccount.accountNumber(), openAccount.initialBalance());
            log.info("Account openend with number {} and InitialBalance {}", openAccount.accountNumber(), openAccount.initialBalance());
            persist(accountOpenedEvent, evt -> {
                updateWith(evt);
                getSender().tell(evt, getSelf());
            });
        } else if (message instanceof DepositFundsCommand) {
            DepositFundsCommand depositFunds = (DepositFundsCommand) message;
            FundsDepositedEvent fundsDepositedEvent = new FundsDepositedEvent(accountState.accountNumber(),depositFunds.amount());
            log.info("Deposit {} funds with balance {}", depositFunds.amount(), accountState.balance());
            persist(fundsDepositedEvent, evt -> {
                updateWith(evt);
                log.info("new Balance is {}", accountState.balance());
                getSender().tell(evt, getSelf());
            });
        }  else if (message instanceof WithdrawFundsCommand) {
            WithdrawFundsCommand withdrawFunds = (WithdrawFundsCommand) message;
            FundsWithdrawnEvent fundsWithdrawnEvent = new FundsWithdrawnEvent(accountState.accountNumber(), withdrawFunds.amount());
            log.info("Withdrawn {} funds with balance {}", withdrawFunds.amount(), accountState.balance());
            persist(fundsWithdrawnEvent, evt -> {
                updateWith(evt);
                log.info("new Balance is {}", accountState.balance());
                getSender().tell(evt, getSelf());
            });
        } else {
            unhandled(message);
        }
    }

    private void updateWith(AccountOpenedEvent event) {
        accountState = new AccountState(event.accountNumber(), event.initialBalance());
    }

    private void updateWith(FundsDepositedEvent event) {
        accountState = accountState.fromDeposited(event.amount());
    }

    private void updateWith(FundsWithdrawnEvent event) {
        accountState = accountState.fromWithdrawn(event.amount());
    }

}
