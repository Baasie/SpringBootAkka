package com.baasie.examples.springbootakka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.baasie.examples.springbootakka.actor.ManageAccount;
import com.baasie.examples.springbootakka.configuration.SpringExtension;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
class Runner implements CommandLineRunner {

    @Autowired
    private ActorSystem actorSystem;

    @Autowired
    private SpringExtension springExtension;

    @Override
    public void run(String[] args) throws Exception {
        log.info("Starting AccountService actorSystem");
        ActorRef accountService = actorSystem.actorOf(springExtension.propsCreate("accountService"), "accountService");
        accountService.tell(new ManageAccount("NL86BUNQ"), null);
    }
}
