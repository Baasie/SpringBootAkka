package com.baasie.examples.springbootakka;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootAkkaApplicationTests {

	@Ignore("For now, working on it......")
	@Test
	public void contextLoads() {
	}

}
